﻿using System;
using NUnit.Framework;
using Services.Helpers;

namespace Tests.Tests
{
    [TestFixture]
    public class DataValueLabelHelperTests
    {
        [Test]
        public void GetLabelFromDateTime_ItShouldReturnLabelAsExpected()
        {
            //Arrange
            var dateTime = new DateTime(2021, 01, 01);

            //Act
            var result = DataValueLabelHelper.GetMonthYearLabel(dateTime);

            //Assert
            Assert.AreEqual("Jan-2021", result);
        }
    }
}
