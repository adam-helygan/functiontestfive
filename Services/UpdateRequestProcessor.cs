﻿using Domain.DTO;
using Domain.Requests;
using Services.Interfaces;

namespace Services
{
    public class UpdateRequestProcessor : IUpdateRequestProcessor
    {
        public DataSetUpdate ProcessRequestForDataSet(string dataSetName, UpdateRequest updateRequest)
        {
            var update = new DataSetUpdate
            {
                DataSetName = dataSetName
            };

            foreach (var dataItem in updateRequest.Data)
            {
                if (dataItem.Name == dataSetName)
                {
                    update.MasterValue = dataItem.Value;
                    update.Timestamp = dataItem.Timestamp;
                }
                else if (dataItem.Name.StartsWith(dataSetName))
                {
                    update.Timestamp = dataItem.Timestamp;
                    update.DiscreteValues.Add(new DataSetUpdateDiscreteValue
                    {
                        Name = dataItem.Name,
                        Value = dataItem.Value,
                        Timestamp = dataItem.Timestamp
                    });
                }
            }

            return update;
        }
    }
}
