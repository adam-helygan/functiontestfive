﻿using Domain.Requests;

namespace Services.Interfaces
{
    public interface IDataSetUpdateOrchestrator
    {
        void Process(UpdateRequest request);
    }
}
