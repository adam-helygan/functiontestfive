﻿using System;
using System.Collections.Generic;
using Domain.Requests;
using NUnit.Framework;
using Services;

namespace Tests.Tests
{
    [TestFixture]
    public class HmacGeneratorTests
    {
        [Test]
        public void GenerateSignature_ItShouldGenerateInExpectedFormat()
        {
            //Arrange
            var date = DateTime.Now;

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = date,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "smets1", Timestamp = date, Value = 1000 },
                    new UpdateRequestDataItem { Name = "smets2", Timestamp = date, Value = 2000 }
                }
            };

            HmacGenerator generator = new HmacGenerator();

            //Act
            var result = generator.GenerateMessageDefinition(request);

            //Assert
            Assert.AreEqual($"{date.ToString("dd-M-yyyy")}smets1smets210002000", result);
        }
    }
}
