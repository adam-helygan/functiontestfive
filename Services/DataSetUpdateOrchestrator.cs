﻿using System.Collections.Generic;
using Domain.Constants;
using Domain.DTO;
using Domain.Requests;
using Services.Interfaces;

namespace Services
{
    public class DataSetUpdateOrchestrator : IDataSetUpdateOrchestrator
    {
        private readonly IUpdateRequestProcessor _updateRequestProcessor;
        private readonly IDataSetService _dataSetService;

        private readonly string[] _dataSetNames = {
            DataSetNameConstants.Smets1,
            DataSetNameConstants.Smets2,
            DataSetNameConstants.ConnectedHomeDevices,
            DataSetNameConstants.DisconnectedHomes
        };

        public DataSetUpdateOrchestrator(IUpdateRequestProcessor updateRequestProcessor, IDataSetService dataSetService)
        {
            _updateRequestProcessor = updateRequestProcessor;
            _dataSetService = dataSetService;
        }

        public void Process(UpdateRequest request)
        {
            List<DataSetUpdate> dataSetUpdates = new List<DataSetUpdate>();

            foreach (var name in _dataSetNames)
            {
                var update = _updateRequestProcessor.ProcessRequestForDataSet(name, request);
                if (update.MasterValue != null || update.DiscreteValues.Count > 0)
                {
                    dataSetUpdates.Add(update);
                }
            }

            foreach (var update in dataSetUpdates)
            {
                _dataSetService.UpdateDataSet(update);
            }
        }
    }
}
