﻿using Domain.DTO;

namespace Services.Interfaces
{
    public interface IValueCalculator
    {
        void Calculate(DataSetUpdate dataSet);
    }
}
