﻿using System;
using System.Security.Cryptography;
using Domain;
using Domain.Requests;

namespace Services
{
    public class HmacGenerator
    {
        public string GenerateMessageDefinition(UpdateRequest request)
        {
            string names = "";
            string values = "";
            foreach (var item in request.Data)
            {
                names += item.Name;
                values += item.Value.ToString();
            }
            
            return request.Timestamp.ToString("dd-M-yyyy") + names + values;
        }

        public string GenerateSignatureHash(string message, string secret)
        {
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);

            using (var hmacsha1 = new HMACSHA1(keyByte))
            {
                byte[] hashmessage = hmacsha1.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }
    }
}
