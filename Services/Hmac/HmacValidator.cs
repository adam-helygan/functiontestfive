﻿using System;
using Domain.Requests;
using Services.Interfaces;

namespace Services
{
    public class HmacValidator : IHmacValidator
    {
        public bool RequestIsValid(UpdateRequest request)
        {
            string secret = Environment.GetEnvironmentVariable("HmacKey");
            HmacGenerator generator = new HmacGenerator();

            string signatureMessage = generator.GenerateMessageDefinition(request);
            string signatureHash = generator.GenerateSignatureHash(signatureMessage, secret);

            if (signatureHash != request.Signature)
            {
                return false;
            }

            return true;
        }
    }
}
