﻿using Castle.Components.DictionaryAdapter;
using Domain.DTO;
using NUnit.Framework;
using Services;

namespace Tests.Tests
{
    [TestFixture]
    public class ValueCalculatorTests
    {
        [Test]
        public void Calculate_WhenHasMasterValueAlready_ItShouldNotCalculate()
        {
            //Arrange
            DataSetUpdate update = new DataSetUpdate
            {
                MasterValue = 1000
            };

            ValueCalculator calculator = new ValueCalculator();

            //Act
            calculator.Calculate(update);

            //Assert
            Assert.AreEqual(1000, update.MasterValue);
        }

        [Test]
        public void Calculate_WhenHasDiscreteValues_ItShouldDefaultToAddingTogether()
        {
            //Arrange
            DataSetUpdate update = new DataSetUpdate
            {
                DiscreteValues = new EditableList<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 1000 },
                    new() { Name = "smets1-gas", Value = 2000 }
                }
            };

            ValueCalculator calculator = new ValueCalculator();

            //Act
            calculator.Calculate(update);

            //Assert
            Assert.AreEqual(3000, update.MasterValue);
        }
    }
}
