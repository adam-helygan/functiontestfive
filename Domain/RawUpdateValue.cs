﻿using System;

namespace Domain
{
    public class RawUpdateValue
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public DateTime Timestamp { get; set; }

        public int DataSetId { get; set; }
        public DccDataSet DataSet { get; set; }
    }
}
