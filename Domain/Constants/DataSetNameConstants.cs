﻿namespace Domain.Constants
{
    public static class DataSetNameConstants
    {
        public const string Smets1 = "smets1";
        public const string Smets2 = "smets2";
        public const string ConnectedHomeDevices = "connected-home-devices";
        public const string DisconnectedHomes = "disconnected-homes";
    }
}
