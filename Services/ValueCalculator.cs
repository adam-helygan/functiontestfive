﻿using System.Linq;
using Domain.DTO;
using Services.Interfaces;

namespace Services
{
    public class ValueCalculator : IValueCalculator
    {
        public void Calculate(DataSetUpdate updateData)
        {
            // Do not attempt calculation if Master Value already set
            if (updateData.MasterValue != null)
            {
                return;
            }

            // Data Set specific calculations here
            // ....

            // Default to summing discrete values
            if (updateData.DiscreteValues.Any())
            {
                updateData.MasterValue = updateData.DiscreteValues.Select(x => x.Value).Sum();
            }
        }
    }
}
