﻿using Domain;
using Domain.DTO;

namespace Services.Interfaces
{
    public interface IStorageValidator
    {
        bool CanBeStored(DccDataSet storedDataSet, DataSetUpdate updateDto);
    }
}
