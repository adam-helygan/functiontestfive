using System;
using System.IO;
using System.Net;
using Domain;
using Domain.Requests;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Services.Interfaces;

namespace FunctionTestFive
{
    public class UpdateData
    {
        private readonly IDataSetUpdateOrchestrator _dataSetUpdateOrchestrator;
        private readonly IHmacValidator _hmacValidator;

        public UpdateData(IDataSetUpdateOrchestrator dataSetUpdateOrchestrator, IHmacValidator hmacValidator)
        {
            _dataSetUpdateOrchestrator = dataSetUpdateOrchestrator;
            _hmacValidator = hmacValidator;
        }

        [Function("UpdateData")]
        public HttpResponseData Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post")] HttpRequestData req,
            FunctionContext executionContext)
        {
            var logger = executionContext.GetLogger("UpdateData");
            logger.LogInformation("C# HTTP trigger function processed a request.");

            string content = new StreamReader(req.Body).ReadToEnd(); // make async
            UpdateRequest request = JsonConvert.DeserializeObject<UpdateRequest>(content);

            if (!_hmacValidator.RequestIsValid(request))
            {
                var forbiddenResponse = req.CreateResponse(HttpStatusCode.Forbidden);
                forbiddenResponse.WriteString("Invalid request");
                return forbiddenResponse;
            }

            _dataSetUpdateOrchestrator.Process(request);

            var response = req.CreateResponse(HttpStatusCode.OK);
            response.Headers.Add("Content-Type", "text/plain; charset=utf-8");

            response.WriteString("Welcome to Azure Functions!");

            return response;
        }
    }
}
