﻿using System.Collections.Generic;
using Domain;
using Domain.DTO;
using NUnit.Framework;
using Services;

namespace Tests.Tests
{
    [TestFixture]
    public class StorageValidatorTests
    {
        [Test]
        public void CanBeStored_IfNotCurrentlyInDatabase_ReturnTrue()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet notInDb = null;

            DataSetUpdate update = new DataSetUpdate();

            //Act
            var result = validator.CanBeStored(notInDb, update);

            //Assert
            Assert.True(result);
        }

        [Test]
        public void CanBeStored_IfHasAMasterValueThatIsHigherThanStored_ReturnTrue()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
                Value = 1000
            };

            DataSetUpdate update = new DataSetUpdate
            {
                MasterValue = 2000
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.True(result);
        }

        [Test]
        public void CanBeStored_IfHasAMasterValueThatIsLowerThanStored_ReturnFalse()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
                Value = 3000
            };

            DataSetUpdate update = new DataSetUpdate
            {
                MasterValue = 2000
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.False(result);
        }

        [Test]
        public void CanBeStored_IfHasADiscreteValuesThatAreHigherThanStored_ReturnTrue()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
                DiscreteValues = new List<DiscreteValue>
                {
                    new() { Label = "smets1-elec", Value = 1000 },
                    new() { Label = "smets1-gas", Value = 1000 }
                }
            };

            DataSetUpdate update = new DataSetUpdate
            {
                DiscreteValues = new List<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 2000 },
                    new() { Name = "smets1-gas", Value = 2000 }
                }
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.True(result);
        }

        [Test]
        public void CanBeStored_IfHasADiscreteValuesThatAreLowerThanStored_ReturnFalse()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
                DiscreteValues = new List<DiscreteValue>
                {
                    new() { Label = "smets1-elec", Value = 3000 },
                    new() { Label = "smets1-gas", Value = 3000 }
                }
            };

            DataSetUpdate update = new DataSetUpdate
            {
                DiscreteValues = new List<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 2000 },
                    new() { Name = "smets1-gas", Value = 2000 }
                }
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.False(result);
        }

        [Test]
        public void CanBeStored_IfHasASingleDiscreteValuesThatAreLowerThanStored_ReturnFalse()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
                DiscreteValues = new List<DiscreteValue>
                {
                    new() { Label = "smets1-elec", Value = 1000 },
                    new() { Label = "smets1-gas", Value = 3000 }
                }
            };

            DataSetUpdate update = new DataSetUpdate
            {
                DiscreteValues = new List<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 2000 },
                    new() { Name = "smets1-gas", Value = 2000 }
                }
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.False(result);
        }

        [Test]
        public void CanBeStored_IfHasANewDiscreteValues_ReturnTrue()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
            };

            DataSetUpdate update = new DataSetUpdate
            {
                DiscreteValues = new List<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 2000 },
                    new() { Name = "smets1-gas", Value = 2000 }
                }
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.True(result);
        }

        [Test]
        public void CanBeStored_IfHasASingleNewDiscreteValue_ReturnTrue()
        {
            //Arrange
            StorageValidator validator = new StorageValidator();
            DccDataSet storedDataSet = new DccDataSet
            {
                DiscreteValues = new List<DiscreteValue>
                {
                    new() { Label = "smets1-elec", Value = 1000 },
                }
            };

            DataSetUpdate update = new DataSetUpdate
            {
                DiscreteValues = new List<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 2000 },
                    new() { Name = "smets1-gas", Value = 2000 }
                }
            };

            //Act
            var result = validator.CanBeStored(storedDataSet, update);

            //Assert
            Assert.True(result);
        }
    }
}
