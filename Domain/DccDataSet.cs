﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class DccDataSet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public DateTime LastUpdated { get; set; }
        public List<DataValue> DataValues { get; set; } = new List<DataValue>();
        public List<DiscreteValue> DiscreteValues { get; set; } = new List<DiscreteValue>();
        public List<RawUpdateValue> RawDataUpdateValues { get; set; } = new List<RawUpdateValue>();
    }
}
