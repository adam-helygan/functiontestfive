using System;
using Microsoft.Azure.Functions.Worker.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;
using Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Services;
using Services.Interfaces;

namespace FunctionTestFive
{
    public class Program
    {
        public static void Main()
        {
            string connectionString = Environment.GetEnvironmentVariable("SqlConnectionString");

            var host = new HostBuilder()
                .ConfigureFunctionsWorkerDefaults()
                .ConfigureServices(s =>
                {
                    s.AddLogging(options => options.AddFilter("Services", LogLevel.Information));

                    s.AddDbContext<DataSetContext>(
                        options => SqlServerDbContextOptionsExtensions.UseSqlServer(options, connectionString)
                    );

                    s.AddScoped<IHmacValidator, HmacValidator>();
                    s.AddScoped<IDataSetUpdateOrchestrator, DataSetUpdateOrchestrator>();
                    s.AddScoped<IUpdateRequestProcessor, UpdateRequestProcessor>();
                    s.AddScoped<IDataSetService, DataSetService>();
                    s.AddScoped<IStorageValidator, StorageValidator>();
                    s.AddScoped<IValueCalculator, ValueCalculator>();
                })
                .Build();

            host.Run();
        }
    }
}