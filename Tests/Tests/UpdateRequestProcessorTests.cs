﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Constants;
using Domain.Requests;
using NUnit.Framework;
using Services;

namespace Tests.Tests
{
    [TestFixture]
    public class UpdateRequestProcessorTests
    {
        [Test]
        public void ProcessRequestForDataSet_ItShouldSetTheName()
        {
            //Arrange
            DateTime dateTime = DateTime.Now;
            UpdateRequestProcessor processor = new UpdateRequestProcessor();

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = dateTime,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "connected-home-devices" }
                }
                
            };

            //Act
            var result = processor.ProcessRequestForDataSet(DataSetNameConstants.ConnectedHomeDevices, request);

            //Assert
            Assert.AreEqual(DataSetNameConstants.ConnectedHomeDevices, result.DataSetName);

        }

        [Test]
        public void ProcessRequestForDataSet_WhenDoesNotHaveDiscreteValuesForCalculation_ItShouldSetTheMasterValue()
        {
            //Arrange
            DateTime dateTime = DateTime.Now;
            UpdateRequestProcessor processor = new UpdateRequestProcessor();

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = dateTime,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "connected-home-devices", Value = 1000 }
                }

            };

            //Act
            var result = processor.ProcessRequestForDataSet(DataSetNameConstants.ConnectedHomeDevices, request);

            //Assert
            Assert.AreEqual(1000, result.MasterValue);
            Assert.AreEqual(false, result.RequiresCalculation());
        }

        [Test]
        public void ProcessRequestForDataSet_WhenContainsDiscreteValuesForCalculation_ItShouldNotSetTheMasterValue()
        {
            //Arrange
            DateTime dateTime = DateTime.Now;
            UpdateRequestProcessor processor = new UpdateRequestProcessor();

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = dateTime,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "smets1-elec", Value = 1000 }
                }
            };

            //Act
            var result = processor.ProcessRequestForDataSet(DataSetNameConstants.Smets1, request);

            //Assert
            Assert.IsNull(result.MasterValue);
            Assert.AreEqual(true, result.RequiresCalculation());
        }

        [Test]
        public void ProcessRequestForDataSet_WhenContainsDiscreteValuesForCalculation_ItShouldNotSetTheDiscreteValues()
        {
            //Arrange
            DateTime dateTime = DateTime.Now;
            UpdateRequestProcessor processor = new UpdateRequestProcessor();

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = dateTime,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "smets1-elec", Value = 1000 },
                    new UpdateRequestDataItem { Name = "smets1-gas", Value = 2000 }
                }
            };

            //Act
            var result = processor.ProcessRequestForDataSet(DataSetNameConstants.Smets1, request);

            //Assert
            Assert.AreEqual(2, result.DiscreteValues.Count());
            Assert.AreEqual(result.DiscreteValues.ElementAt(0).Name, "smets1-elec");
            Assert.AreEqual(result.DiscreteValues.ElementAt(0).Value, 1000);
            Assert.AreEqual(result.DiscreteValues.ElementAt(1).Name, "smets1-gas");
            Assert.AreEqual(result.DiscreteValues.ElementAt(1).Value, 2000);
        }

        [Test]
        public void ProcessRequestForDataSet_WhenRequestContainsValuesNotRelatedToDataSet_ItShouldNotAddThemToDiscreteValues()
        {
            //Arrange
            DateTime dateTime = DateTime.Now;
            UpdateRequestProcessor processor = new UpdateRequestProcessor();

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = dateTime,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "smets2-elec", Value = 1000 },
                    new UpdateRequestDataItem { Name = "smets2-gas", Value = 2000 }
                }
            };

            //Act
            var result = processor.ProcessRequestForDataSet(DataSetNameConstants.Smets1, request);

            //Assert
            Assert.AreEqual(0, result.DiscreteValues.Count());
        }
    }
}
