﻿using System;
using System.Collections.Generic;
using Domain.Requests;
using NUnit.Framework;
using Services;

namespace Tests.Tests
{
    [TestFixture]
    public class HmacValidatorTests
    {
        [Test]
        public void RequestIsValid_WhenSignatureMatches_ItShouldReturnTrue()
        {
            //Arrange
            var secret = "testKEYjXQHg37t21YosbA==";
            Environment.SetEnvironmentVariable("HmacKey", secret);
            HmacGenerator generator = new HmacGenerator();

            var date = DateTime.Now;

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = date,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "smets1", Timestamp = date, Value = 1000 },
                    new UpdateRequestDataItem { Name = "smets2", Timestamp = date, Value = 2000 }
                }
            };

            // generate valid signature
            var message = generator.GenerateMessageDefinition(request);
            request.Signature = generator.GenerateSignatureHash(message, secret);

            HmacValidator hmacValidator = new HmacValidator();

            //Act
            var result = hmacValidator.RequestIsValid(request);

            //Assert
            Assert.AreEqual(true, result);
        }

        [Test]
        public void RequestIsValid_WhenSignatureDoesNotMatch_ItShouldReturnFalse()
        {
            //Arrange
            var secret = "testKEYjXQHg37t21YosbA==";
            Environment.SetEnvironmentVariable("HmacKey", secret);
            HmacGenerator generator = new HmacGenerator();

            var date = DateTime.Now;

            UpdateRequest request = new UpdateRequest
            {
                Timestamp = date,
                Data = new List<UpdateRequestDataItem>
                {
                    new UpdateRequestDataItem { Name = "smets1", Timestamp = date, Value = 1000 },
                    new UpdateRequestDataItem { Name = "smets2", Timestamp = date, Value = 2000 }
                }
            };

            // generate valid signature
            request.Signature = "someIncorrectSignature";

            HmacValidator hmacValidator = new HmacValidator();

            //Act
            var result = hmacValidator.RequestIsValid(request);

            //Assert
            Assert.AreEqual(false, result);
        }
    }
}
