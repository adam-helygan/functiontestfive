﻿using System;

namespace Domain
{
    public class DiscreteValue
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public double Value { get; set; }
        public DateTime LastUpdated { get; set; }

        public int DataSetId { get; set; }
        public DccDataSet DataSet { get; set; }
    }
}
