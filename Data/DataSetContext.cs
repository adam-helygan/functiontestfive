﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class DataSetContext : DbContext
    {
        public DataSetContext(DbContextOptions<DataSetContext> options)
            : base(options)
        {
        }

        public DbSet<DccDataSet> DataSets { get; set; }

        public DbSet<DataValue> DataValues { get; set; }

        public DbSet<DiscreteValue> DiscreteValues { get; set; }

        public DbSet<RawUpdateValue> RawUpdateValues { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=FunctionsSandbox;User ID=FunctionsSandboxUser;Password=password123");
        }
    }
}
