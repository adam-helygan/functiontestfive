﻿using System;
using System.Collections.Generic;

namespace Domain.Requests
{
    public class UpdateRequest
    {
        public DateTime Timestamp { get; set; }
        public string Signature { get; set; }
        public List<UpdateRequestDataItem> Data { get; set; } = new List<UpdateRequestDataItem>();
    }

    public class UpdateRequestDataItem
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
