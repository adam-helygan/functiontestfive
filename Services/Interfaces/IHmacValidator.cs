﻿using Domain;
using Domain.Requests;

namespace Services.Interfaces
{
    public interface IHmacValidator
    {
        bool RequestIsValid(UpdateRequest request);
    }
}
