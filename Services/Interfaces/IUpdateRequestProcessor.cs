﻿using Domain.DTO;
using Domain.Requests;

namespace Services.Interfaces
{
    public interface IUpdateRequestProcessor
    {
        DataSetUpdate ProcessRequestForDataSet(string dataSetName, UpdateRequest updateRequest);
    }
}
