﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using Domain;
using Domain.DTO;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using Services;
using Services.Interfaces;

namespace Tests.Tests
{
    [TestFixture]
    public class DataSetServiceTests : DbTestBase
    {
        IValueCalculator _calculator;
        IStorageValidator _storageValidator;
        ILogger<DataSetService> _logger;

        [SetUp]
        public void SetUp()
        {
            _calculator = new ValueCalculator();
            _storageValidator = Substitute.For<IStorageValidator>();
            _logger = Substitute.For<ILogger<DataSetService>>();
        }

        [Test]
        public void UpdateDataSet_WhenDataItemNotPresentInDatabase_ItShouldCreate()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeNow = DateTime.Now;

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeNow,
                DataSetName = "connected-home-devices",
                MasterValue = 1000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual("connected-home-devices", result.Name);
            Assert.That(dateTimeNow, Is.EqualTo(result.LastUpdated).Within(2).Seconds);
            Assert.AreEqual(1000, result.Value);
        }

        [Test]
        public void UpdateDataSet_WhenDataItemNotPresentInDatabase_ItShouldCreateDiscreteValues()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeNow = DateTime.Now;

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeNow,
                DataSetName = "smets1",
                DiscreteValues = new List<DataSetUpdateDiscreteValue>
                {
                    new() { Name = "smets1-elec", Value = 1000, Timestamp = dateTimeNow },
                    new() { Name = "smets1-gas", Value = 2000, Timestamp = dateTimeNow }
                }
                
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = context.DataSets.SingleOrDefault(x => x.Name == "smets1");

            //Assert
            Assert.AreEqual("smets1", result.Name);
            Assert.AreEqual("smets1-elec", result.DiscreteValues.ElementAt(0).Label);
            Assert.AreEqual(1000, result.DiscreteValues.ElementAt(0).Value);
            Assert.AreEqual("smets1-gas", result.DiscreteValues.ElementAt(1).Label);
            Assert.AreEqual(2000, result.DiscreteValues.ElementAt(1).Value);
        }

        [Test]
        public void UpdateDataSet_WhenExistsInDatabase_ItShouldUpdateValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeNow = DateTime.Now;

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = DateTime.Now.AddDays(-7),
                Name = "connected-home-devices",
                Value = 1000
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeNow,
                DataSetName = "connected-home-devices",
                MasterValue = 2000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.That(dateTimeNow, Is.EqualTo(result.LastUpdated).Within(2).Seconds);
            Assert.AreEqual(2000, result.Value);
        }

        [Test]
        public void UpdateDataSet_WhenValueIsLowerThanStored_ItShouldNotUpdateValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeNow = DateTime.Now;

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = DateTime.Now.AddDays(-7),
                Name = "connected-home-devices",
                Value = 2000
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeNow,
                DataSetName = "connected-home-devices",
                MasterValue = 1999
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(2000, result.Value);
        }

        [Test]
        public void UpdateDataSet_WhenExistsInDatabaseWithDiscreteValues_ItShouldShouldUpdateAsExpected()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeNow = DateTime.Now;
            var oneWeekAgo = DateTime.Now.AddDays(-7);

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = oneWeekAgo,
                Name = "smets1",
                Value = 3000,
                DiscreteValues = new List<DiscreteValue>
                {
                    new() { Label = "smets1-elec", Value = 1000, LastUpdated = oneWeekAgo },
                    new() { Label = "smets1-gas", Value = 2000, LastUpdated = oneWeekAgo }
                }
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeNow,
                DataSetName = "smets1",
                DiscreteValues = new List<DataSetUpdateDiscreteValue>()
                {
                    new() { Name = "smets1-elec", Value = 2000, Timestamp = dateTimeNow },
                    new() { Name = "smets1-gas", Value = 3000, Timestamp = dateTimeNow }
                }
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "smets1");

            //Assert
            Assert.AreEqual(2, result.DiscreteValues.Count);
            Assert.AreEqual("smets1-elec", result.DiscreteValues.ElementAt(0).Label);
            Assert.AreEqual(2000, result.DiscreteValues.ElementAt(0).Value);
            Assert.AreEqual("smets1-gas", result.DiscreteValues.ElementAt(1).Label);
            Assert.AreEqual(3000, result.DiscreteValues.ElementAt(1).Value);
        }

        [Test]
        public void UpdateDataSet_WhenDataItemNotPresentInDatabase_ItShouldSetTheMonthlyDataValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var datetime = new DateTime(2021, 01, 01);

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = datetime,
                DataSetName = "connected-home-devices",
                MasterValue = 1000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(1, result.DataValues.Count);
            Assert.AreEqual("Jan-2021", result.DataValues.ElementAt(0).Label);
            Assert.AreEqual(1000, result.DataValues.ElementAt(0).Value);
        }

        [Test]
        public void UpdateDataSet_WhenMonthlyValueAlreadySet_ItShouldUpdateValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeStored = new DateTime(2021, 01, 01);
            var dateTimeUpdate = new DateTime(2021, 01, 21);

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = dateTimeStored,
                Name = "connected-home-devices",
                Value = 1000,
                DataValues = new List<DataValue>
                {
                    new() { Label = "Jan-2021", Value = 1000 }
                }
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeUpdate,
                DataSetName = "connected-home-devices",
                MasterValue = 2000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(1, result.DataValues.Count);
            Assert.AreEqual("Jan-2021", result.DataValues.ElementAt(0).Label);
            Assert.AreEqual(2000, result.DataValues.ElementAt(0).Value);
        }

        [Test]
        public void UpdateDataSet_WhenMonthlyValueAlreadySetButUpdateValueIsLower_ItShouldNotUpdateValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeStored = new DateTime(2021, 01, 01);
            var dateTimeUpdate = new DateTime(2021, 01, 21);

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = dateTimeStored,
                Name = "connected-home-devices",
                Value = 1000,
                DataValues = new List<DataValue>
                {
                    new() { Label = "Jan-2021", Value = 1000 }
                }
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeUpdate,
                DataSetName = "connected-home-devices",
                MasterValue = 500
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(1, result.DataValues.Count);
            Assert.AreEqual("Jan-2021", result.DataValues.ElementAt(0).Label);
            Assert.AreEqual(1000, result.DataValues.ElementAt(0).Value);
        }

        [Test]
        public void UpdateDataSet_WhenUpdatingANewMonth_ItShouldCreateTheNewMonthValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var dateTimeStored = new DateTime(2021, 01, 01);
            var dateTimeUpdate = new DateTime(2021, 02, 01);

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = dateTimeStored,
                Name = "connected-home-devices",
                Value = 1000,
                DataValues = new List<DataValue>
                {
                    new() { Label = "Jan-2021", Value = 1000 }
                }
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = dateTimeUpdate,
                DataSetName = "connected-home-devices",
                MasterValue = 2000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(2, result.DataValues.Count);
            Assert.AreEqual("Jan-2021", result.DataValues.ElementAt(0).Label);
            Assert.AreEqual(1000, result.DataValues.ElementAt(0).Value);
            Assert.AreEqual("Feb-2021", result.DataValues.ElementAt(1).Label);
            Assert.AreEqual(2000, result.DataValues.ElementAt(1).Value);
        }

        [Test]
        public void UpdateDataSet_WhenDataItemNotPresentInDatabase_ItShouldSetTheRawValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var datetime = new DateTime(2021, 01, 01);

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = datetime,
                DataSetName = "connected-home-devices",
                MasterValue = 1000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(1, result.RawDataUpdateValues.Count);
            Assert.AreEqual(1000, result.RawDataUpdateValues.ElementAt(0).Value);
        }

        [Test]
        public void UpdateDataSet_WhenRawValuesPresentInDatabase_ItShouldAddTheRawValue()
        {
            //Arrange
            using var context = new DataSetContext(_dbOptions);
            _storageValidator.CanBeStored(default, default).ReturnsForAnyArgs(true);
            var datetime = new DateTime(2021, 01, 01);

            context.DataSets.Add(new DccDataSet
            {
                LastUpdated = datetime,
                Name = "connected-home-devices",
                Value = 1000,
                RawDataUpdateValues = new List<RawUpdateValue>
                {
                    new() { Value = 1000, Timestamp = datetime }
                }
            });
            context.SaveChanges();

            var dataSetItem = new DataSetUpdate()
            {
                Timestamp = DateTime.Now,
                DataSetName = "connected-home-devices",
                MasterValue = 2000
            };

            //Act
            var service = new DataSetService(context, _calculator, _storageValidator, _logger);
            service.UpdateDataSet(dataSetItem);

            var result = FetchResultFromDb(context, "connected-home-devices");

            //Assert
            Assert.AreEqual(2, result.RawDataUpdateValues.Count);
            Assert.AreEqual(2000, result.RawDataUpdateValues.ElementAt(1).Value);
        }
    }
}
