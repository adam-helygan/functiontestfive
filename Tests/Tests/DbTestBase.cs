﻿using Data;
using Domain;
using System.Linq;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Tests.Tests
{
    public class DbTestBase
    {
        protected DbContextOptions<DataSetContext> _dbOptions;
        protected SqliteConnection _dbConnection;

        [SetUp]
        public void BaseSetUp()
        {
            _dbConnection = new SqliteConnection("DataSource=:memory:");
            _dbConnection.Open();

            _dbOptions = new DbContextOptionsBuilder<DataSetContext>().UseSqlite(_dbConnection).Options;

            using (var context = new DataSetContext(_dbOptions))
            {
                context.Database.EnsureCreated();
            }
        }

        [TearDown]
        public void BaseTearDown()
        {
            _dbConnection.Dispose();
            using var context = new DataSetContext(_dbOptions);
            context.Database.EnsureDeleted();
        }

        protected DccDataSet? FetchResultFromDb(DataSetContext context, string dataSetName)
        {
            return context.DataSets
                .Include(x => x.DiscreteValues)
                .Include(x => x.DataValues)
                .Include(x => x.RawDataUpdateValues)
                .SingleOrDefault(x => x.Name == dataSetName);
        }
    }
}
