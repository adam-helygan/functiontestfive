﻿using System.Collections.Generic;
using System.Linq;
using Data;
using Domain;
using Domain.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Services.Helpers;
using Services.Interfaces;

namespace Services
{
    public class DataSetService : IDataSetService
    {
        private readonly DataSetContext _dbContext;
        private readonly IValueCalculator _valueCalculator;
        private readonly IStorageValidator _storageValidator;
        private readonly ILogger<DataSetService> _logger;

        public DataSetService(DataSetContext dbContext, IValueCalculator valueCalculator, 
            IStorageValidator storageValidator, ILogger<DataSetService> logger)
        {
            _dbContext = dbContext;
            _valueCalculator = valueCalculator;
            _storageValidator = storageValidator;
            _logger = logger;
        }

        public void UpdateDataSet(DataSetUpdate data)
        {
            _logger.Log(LogLevel.Information, $"Updating Dataset: {data.DataSetName}");

            var storedDataSet = _dbContext.DataSets
                .Include(x => x.DiscreteValues)
                .Include(x => x.DataValues)
                .Include(x => x.RawDataUpdateValues)
                .SingleOrDefault(x => x.Name == data.DataSetName);

            if (!_storageValidator.CanBeStored(storedDataSet, data))
            {
                _logger.Log(LogLevel.Error, $"Cannot data Dataset: {data.DataSetName}. Value provided is lower or equal to stored value.");
                return;
            }

            if (data.RequiresCalculation())
            {
                _valueCalculator.Calculate(data);
            }

            if (storedDataSet == null)
            {
                Create(data);
            }
            else
            {
                Update(data, storedDataSet);
            }

            _dbContext.SaveChanges();
        }

        private void Create(DataSetUpdate data)
        {
            if (data.MasterValue == null) return;
            
            var dataSet = new DccDataSet
            {
                Name = data.DataSetName,
                Value = data.MasterValue.Value,
                LastUpdated = data.Timestamp,
                DiscreteValues = data.DiscreteValues.Select(x => new DiscreteValue
                {
                    Label = x.Name,
                    Value = x.Value,
                    LastUpdated = x.Timestamp
                }).ToList(),
                DataValues = new List<DataValue>
                {
                    new DataValue()
                    {
                        Label = DataValueLabelHelper.GetMonthYearLabel(data.Timestamp),
                        Value = data.MasterValue.Value
                    }
                }
            };
            dataSet.RawDataUpdateValues.Add(new RawUpdateValue
            {
                Value = data.MasterValue.Value,
                Timestamp = data.Timestamp
            });
            _dbContext.DataSets.Add(dataSet);
        }

        private void Update(DataSetUpdate data, DccDataSet dataSet)
        {
            if (data.MasterValue != null && data.MasterValue > dataSet.Value)
            {
                dataSet.Value = data.MasterValue.Value;
                dataSet.LastUpdated = data.Timestamp;

                var monthlyDataValueLabel = DataValueLabelHelper.GetMonthYearLabel(data.Timestamp);
                var storedMonthValue = dataSet.DataValues.SingleOrDefault(x => x.Label == monthlyDataValueLabel);
                if (storedMonthValue != null)
                {
                    storedMonthValue.Value = data.MasterValue.Value;
                }
                else
                {
                    dataSet.DataValues.Add(new DataValue
                    {
                        Label = monthlyDataValueLabel,
                        Value = data.MasterValue.Value
                    });
                }

                foreach (var discreteValue in dataSet.DiscreteValues)
                {
                    var valueToUpdate = data.DiscreteValues.FirstOrDefault(x => x.Name == discreteValue.Label);
                    if (valueToUpdate != null)
                    {
                        discreteValue.Value = valueToUpdate.Value;
                        discreteValue.LastUpdated = valueToUpdate.Timestamp;
                    }
                }

                dataSet.RawDataUpdateValues.Add(new RawUpdateValue
                {
                    Value = data.MasterValue.Value,
                    Timestamp = data.Timestamp
                });
            }
        }
    }
}
