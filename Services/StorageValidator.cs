﻿using System.Linq;
using Domain;
using Domain.DTO;
using Services.Interfaces;

namespace Services
{
    public class StorageValidator : IStorageValidator
    {
        public bool CanBeStored(DccDataSet storedDataSet, DataSetUpdate updateDto)
        {
            // New Update - not currently in DB
            if (storedDataSet == null)
            {
                return true;
            }

            // Check that Master Value is higher than stored
            if (updateDto.MasterValue != null && updateDto.MasterValue > storedDataSet.Value)
            {
                return true;
            }

            // If no Master Value - Check that *all* Discrete Values are higher than stored
            if (updateDto.MasterValue == null && updateDto.DiscreteValues.Any())
            {
                int invalidUpdateCount = 0;
                foreach (var discreteValue in updateDto.DiscreteValues)
                {
                    if (storedDataSet.DiscreteValues.Any(stored =>
                        stored.Label == discreteValue.Name && stored.Value > discreteValue.Value))
                    {
                        invalidUpdateCount++;
                    }
                }

                return invalidUpdateCount == 0;
            }

            return false;
        }
    }
}
