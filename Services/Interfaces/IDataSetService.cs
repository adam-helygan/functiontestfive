﻿using Domain;
using Domain.DTO;
using Domain.Requests;

namespace Services.Interfaces
{
    public interface IDataSetService
    {
        void UpdateDataSet(DataSetUpdate data);
    }
}
