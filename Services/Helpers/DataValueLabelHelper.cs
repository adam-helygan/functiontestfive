﻿using System;

namespace Services.Helpers
{
    public static class DataValueLabelHelper
    {
        public static string GetMonthYearLabel(DateTime dateTime)
        {
            return $"{dateTime:MMM}-{dateTime:yyyy}";
        }
    }
}
