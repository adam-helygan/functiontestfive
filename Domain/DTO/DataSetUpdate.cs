﻿using System;
using System.Collections.Generic;

namespace Domain.DTO
{
    public class DataSetUpdate
    {
        public string DataSetName { get; set; }

        public double? MasterValue { get; set; }

        public List<DataSetUpdateDiscreteValue> DiscreteValues { get; set; } = new List<DataSetUpdateDiscreteValue>();
        public DateTime Timestamp { get; set; }

        public bool RequiresCalculation()
        {
            return MasterValue == null;
        }
    }

    public class DataSetUpdateDiscreteValue
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
